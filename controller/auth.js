const userRepo = require('../repositories/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { secret } = require('../config/auth');
const moment = require('moment');

async function login(req, res) {
  const user = await userRepo.getUserByEmail(req.body.email);

  if (user) {
    const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

    if (!passwordIsValid) {
      return res.status(400).json({ message: 'invalid email or password'});
    }

    const signature = { 
      id: user.id, 
      name: user.name, 
      email: user.email
    }

    const token = jwt.sign(signature, secret, {
        expiresIn: 86400 // 24 hours
      }
    );

    res.status(200).json({
      message: 'login successfully',
      data: {
        token: token,
        expirerIn: moment.utc().tz('Asia/Jakarta').add(1, 'day').format(),
        user: signature
      }
    })


  } else {
    res.status(400).json({ message: 'invalid email or password'});
  }
}

module.exports = {
  login
}
