const smsRepo = require('../repositories/sms');
const { metaPagination } = require('../helper/formater');
const moment = require('moment');

async function listSms(req, res) {
  let filter = {
    offset: req.offset,
    perPage: req.perPage,
    sortBy: req.sortBy,
    sortType: req.sortType,
  };

  if (
    req.query.startDate !== undefined && moment(req.query.startDate.trim()).isValid() && 
    req.query.endDate !== undefined && moment(req.query.endDate.trim()).isValid()
  ) {
    filter = Object.assign(filter, {
      startDate: req.query.startDate,
      endDate: req.query.endDate
    });
  }

  if (req.query.status !== undefined && req.query.status !== "") {
    filter = Object.assign(filter, {
      status: req.query.status
    });
  }

  const total = await smsRepo.getLogSMS({...filter, query: 'count'});
  const data = await smsRepo.getLogSMS({...filter, query: 'data'});

  return res.status(200)
    .json({
      data: data,
      meta: metaPagination(req.page, req.perPage, total, data.length)
    });
}

module.exports = {
  listSms
}
