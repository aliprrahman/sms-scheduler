const scheduleRepo = require('../repositories/schedule');
const { metaPagination } = require('../helper/formater');
const moment = require('moment');

async function listSchedules(req, res) {
  let filter = {
    offset: req.offset,
    perPage: req.perPage,
    sortBy: req.sortBy,
    sortType: req.sortType,
  };

  if (
    req.query.startDate !== undefined && moment(req.query.startDate.trim()).isValid() && 
    req.query.endDate !== undefined && moment(req.query.endDate.trim()).isValid()
  ) {
    filter = Object.assign(filter, {
      startDate: req.query.startDate,
      endDate: req.query.endDate
    });
  }

  const total = await scheduleRepo.getSchedules({...filter, query: 'count'});
  const data = await scheduleRepo.getSchedules({...filter, query: 'data'});

  return res.status(200)
    .json({
      data: data,
      meta: metaPagination(req.page, req.perPage, total, data.length)
    });
}

async function storeNewSchedule(req, res) {
  const schedule = await scheduleRepo.createSchedule(req.body.runAt, req.body.message);

  if (schedule) {
    res.status(200).json({
      message: 'schedule created successfully',
      data: schedule
    })
  } else {
    res.status(400).json({
      message: 'failed to create schedule'
    })
  }
}

async function newRecepient(req, res) {
  const recepient = await scheduleRepo.addRecepient(req.body.scheduleId, req.body.phoneNumber);

  if (recepient) {
    res.status(200).json({
      message: 'recepient added successfully',
      data: recepient
    })
  } else {
    res.status(400).json({
      message: 'failed to add recepient'
    })
  }
}

module.exports = {
  listSchedules,
  storeNewSchedule,
  newRecepient
}
