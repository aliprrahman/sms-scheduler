const express = require('express');
const router = express.Router();
const { pagination } = require('../middleware/pagination');
const { authentication } = require('../middleware/authentication');
const { validateCreateSchedule, validateRecepient, validateLogin } = require('../middleware/validation');
const scheduleController = require('../controller/schedule');
const smsController = require('../controller/sms');
const authController = require('../controller/auth');

router.get('/schedules', pagination, scheduleController.listSchedules);
router.post('/schedules', [ authentication, validateCreateSchedule ], scheduleController.storeNewSchedule);
router.post('/schedules/add-recepient', [ authentication, validateRecepient ], scheduleController.newRecepient);
router.get('/sms', pagination, smsController.listSms);
router.post('/login', validateLogin, authController.login);

module.exports = router;
