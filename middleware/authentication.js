const jwt = require('jsonwebtoken');
const config = require('../config/auth');

exports.authentication = (req, res, next) => {
  const token = req.headers['authorization'];
	if (!token) {
		// if there is no token
    res.status(401).json({
      message: 'No token provided.'
    });
	}
  const bearer = token.split(" ");
	if (bearer[0] != 'Bearer') {
    res.status(401).json({
      message: 'Unauthorized'
    });
	}
  jwt.verify(bearer[1], config.secret, (err, decoded) => {
		if (err) {
      return res.status(401).json({
        message: 'Unauthorized'
      });
		}
		req.user = decoded;
		next();
	});
}
