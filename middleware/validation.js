const Joi = require('joi');

exports.validateCreateSchedule = (req, res, next) => {
  const schema = Joi.object().keys({
    message: Joi.string().required(),
    runAt: Joi.string().regex(/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/).required(),
  });

  const result = schema.validate(req.body)

  if(result.error !== undefined) {
    return res.status(400).json({
      message: "data invalid",
      error: result.error.details[0].message
    })
  }

  next();
};

exports.validateRecepient = (req, res, next) => {
  const schema = Joi.object().keys({
    phoneNumber: Joi.string().required(),
    scheduleId: Joi.number().required(),
  });

  const result = schema.validate(req.body)

  if(result.error !== undefined) {
    return res.status(400).json({
      message: "data invalid",
      error: result.error.details[0].message
    })
  }

  next();
};

exports.validateLogin = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });

  const result = schema.validate(req.body)

  if(result.error !== undefined) {
    return res.status(400).json({
      message: "data invalid",
      error: result.error.details[0].message
    })
  }

  next();
};
