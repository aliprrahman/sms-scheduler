'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('sms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      scheduleId: {
        type: Sequelize.INTEGER,
        references: {
          key: 'id',
          model: 'schedules',
        },
      },
      recepientId: {
        type: Sequelize.INTEGER,
        references: {
          key: 'id',
          model: 'recepients',
        },
      },
      messageId: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING
      },
      deliveryTime: {
        type: Sequelize.DATE,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('sms');
  }
};
