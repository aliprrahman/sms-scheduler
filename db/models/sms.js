'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Schedule, { foreignKey: 'scheduleId' })
      this.belongsTo(models.Recepient, { foreignKey: 'recepientId' })
    }
  };
  Sms.init({
    scheduleId: DataTypes.INTEGER,
    recepientId: DataTypes.INTEGER,
    messageId: DataTypes.STRING,
    status: DataTypes.STRING,
    deliveryTime: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Sms',
  });
  return Sms;
};
