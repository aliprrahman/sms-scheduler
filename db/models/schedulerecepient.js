'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ScheduleRecepient extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Schedule, { foreignKey: 'scheduleId' })
      this.belongsTo(models.Recepient, { foreignKey: 'recepientId' })
    }
  };
  ScheduleRecepient.init({
    scheduleId: DataTypes.INTEGER,
    recepientId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'ScheduleRecepient',
    tableName: 'schedule_recepients'
  });
  return ScheduleRecepient;
};
