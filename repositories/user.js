const db = require('../db/models/index');

async function getUserByEmail(email) {
  return db.User.findOne({ 
    where: { email: email }
  })
}

module.exports = {
  getUserByEmail
}
