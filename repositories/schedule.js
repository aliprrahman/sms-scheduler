const db = require('../db/models/index');
const { Op } = require('sequelize');
const moment = require('moment');

async function getSchedules(filter) {
  let condition = {};
  if (filter.startDate !== undefined && filter.endDate !== undefined) {
    condition = {
      createdAt: {
        [Op.gte]: moment.utc(filter.startDate).startOf('day'),
        [Op.lte]: moment.utc(filter.endDate).endOf('day')
      }
    }
  }
  if (filter.query == 'count') {
    return db.Schedule.count({
      where: condition
    })
  } else {
    return db.Schedule.findAll({
      attributes: ['id','runAt','message','createdAt'],
      where: condition,
      offset: filter.offset,
      limit: filter.perPage,
      order: [
        [filter.sortBy, filter.sortType]
      ]
    })
  }
}

async function createSchedule(runAt, message) {
  return db.Schedule.create({
    runAt: runAt,
    message: message,
  })
}

async function addRecepient(scheduleId, phoneNumber) {
  const t = await db.sequelize.transaction();

  try {
    let schedule;
    // check phone number
    let phone = await db.Recepient.findOne({ 
      where: { phoneNumber: phoneNumber }
    })

    if (phone) {
      // check phone number have added to schedule
      schedule = db.ScheduleRecepient.findOne({
        where: {
          scheduleId: scheduleId,
          recepientId: phone.id
        }
      })
      if (!schedule) {  
        schedule = await db.ScheduleRecepient.create({
          scheduleId: scheduleId,
          recepientId: phone.id
        }, {
          transaction: t
        })
      }
    } else {
      // store new phone number
      phone = await db.Recepient.create({
        phoneNumber: phoneNumber
      }, {
        transaction: t
      });
      // add phone number to schedule recepient
      schedule = await db.ScheduleRecepient.create({
        scheduleId: scheduleId,
        recepientId: phone.id
      }, {
        transaction: t
      })
    }

    await t.commit();
    return schedule;
  } catch (error) {
    await t.rollback();
    console.log(error)
    return null
  }
}

async function listSchedules(time) {
  return db.Schedule.findAll({
    where: {
      runAt: time,
    },
    include: [
      {
        model: db.Recepient,
        required: true,
      }
    ]
  })
}

module.exports = {
  getSchedules,
  createSchedule,
  addRecepient,
  listSchedules
}
