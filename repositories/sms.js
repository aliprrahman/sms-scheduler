const db = require('../db/models/index');
const { Op } = require('sequelize');
const moment = require('moment');

async function getLogSMS(filter) {
  let condition = {};
  if (filter.startDate !== undefined && filter.endDate !== undefined) {
    condition = {
      createdAt: {
        [Op.gte]: moment.utc(filter.startDate).startOf('day'),
        [Op.lte]: moment.utc(filter.endDate).endOf('day')
      }
    }
  }
  if (filter.status !== undefined) {
    condition = {
      status: filter.status,
    }
  }
  if (filter.query == 'count') {
    return db.Sms.count({
      where: condition
    })
  } else {
    return db.Sms.findAll({
      attributes: ['id','status','messageId','deliveryTime','createdAt'],
      include: [
          {
            model: db.Schedule,
            required: true,
            attributes: ['runAt','message']
          },
          {
            model: db.Recepient,
            required: true,
            attributes: ['phoneNumber']
          }
      ],
      where: condition,
      offset: filter.offset,
      limit: filter.perPage,
      order: [
        [filter.sortBy, filter.sortType]
    ]
    })
  }
}

async function saveLog(scheduleId, recepientId, messageId) {
  return db.Sms.create({
    scheduleId: scheduleId,
    recepientId: recepientId,
    messageId: messageId,
  })
}

async function updateStatus(messageId, status, deliveryTime) {
  const sms = await db.Sms.findOne({
    where: {
      messageId: messageId,
    }
  });

  if (sms) {
    sms.update({
      status: status,
      deliveryTime: deliveryTime
    });
  }
}

async function getSmsNeedToUpdate() {
  return db.Sms.findAll({
    where: {
      [Op.or]: [
        {
          status: null
        },
        {
          status: 'ACCEPTD'
        },
      ]
    }
  })
}

module.exports = {
  getLogSMS,
  saveLog,
  updateStatus,
  getSmsNeedToUpdate
}
