
# SMS Scheduler

app to send sms 

#### Project setup
```
npm install
```

#### Run migration
```
sequelize db:migrate
```

#### Run seeder
```
sequelize db:seed:all 
```

#### API Doc
```
https://documenter.getpostman.com/view/18224943/UVC2H9Yc
```

#### demo user
```
email: admin@gmail.com
password: qwerty123!
```
