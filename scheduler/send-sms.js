const cron = require('node-cron');
const scheduleRepo = require('../repositories/schedule');
const smsRepo = require('../repositories/sms');
const { sms_url } = require('../config/app');
const axios = require('axios');
const moment = require('moment');

// run every 5 minutes
cron.schedule('*/5 * * * *', async () => {
  // send sms schedules
  const currentTime = moment().format('HH:mm:ss')
  const schedules = await scheduleRepo.listSchedules(currentTime)
  for (let i = 0; i < schedules.length; i++) {
    const schedule = schedules[i];
    const recepients = schedule.Recepients.map(recepient => recepient.phoneNumber).join(',')
    axios.post(`${sms_url}/api`, {
      dnis: recepients,
      message: schedule.message
    }).then(response => {
      const results = response.data
      for (let y = 0; y < results.length; y++) {
        const result = results[y];
        const recepient = schedule.Recepients.find(phone => phone.phoneNumber == result.dnis);
        smsRepo.saveLog(schedule.id, recepient.id, result.message_id)
      }
    })
  }
});

// run every midnite
cron.schedule('0 0 * * *', async () => {
  // update status sms
  const sms = await smsRepo.getSmsNeedToUpdate();

  for (let i = 0; i < sms.length; i++) {
    const log = sms[i];
    axios.get(`${sms_url}/api`, {
      params: {
        messageId: log.messageId
      }
    }).then(response => {
        console.log(response.data)
        const deliveryTime = moment.unix(response.data.delivery_time).format("YYYY-MM-DD HH:mm:ss")
        smsRepo.updateStatus(log.messageId, response.data.status, deliveryTime);
      })
  }
});
