const express = require('express')
const app = express()
const morgan = require('morgan')

// load .env file as environment
require('dotenv').config();

// parse requests of content-type - application/json
app.use(express.json());

// log request
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

// run cron job
require('./scheduler');

// load routes
app.use('/', require('./routes/routes'));

app.listen(process.env.APP_PORT, () => {
  console.log(`app listening at http://localhost:${process.env.APP_PORT}`)
})
